#ifndef DISPLAY_H
#define DISPLAY_H

extern "C" {
    bool DefaultBusInit( void );
    void displayInitTDLF( void );
    void displayLink( char *myStr_IP  );
    void displayUseESPTouch( void );
    void displaySetInvert( int setValue);
    void displaySession ( int clients, char *phases, char *tmpOHbuf, char *current_phase_step );
}
#endif 