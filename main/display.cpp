#include "display.h"

///////////// I2C Display ////////////
// SDA GPIO_NUM_25 (D2), SCL GPIO_NUM_33 (D1) // or SDA 21 SCL 22 // set values in menuconfig!

extern "C" {
    #include "ssd1306.h"
    #include "ssd1306_draw.h"
    #include "ssd1306_font.h"
    #include "ssd1306_default_if.h"

    char buf[20]; // BPM display
    char compte[8];
    char top[20]; // used in displaySession()

    static const int I2CDisplayAddress = 0x3C;
    static const int I2CDisplayWidth = 128; // wemos oled screen width and height
    static const int I2CDisplayHeight = 64; 
    static const int I2CResetPin = -1;

    struct SSD1306_Device I2CDisplay;

    bool DefaultBusInit( void ){  
        assert( SSD1306_I2CMasterInitDefault( ) == true );
        assert( SSD1306_I2CMasterAttachDisplayDefault( &I2CDisplay, I2CDisplayWidth, I2CDisplayHeight, I2CDisplayAddress, I2CResetPin ) == true );
    return true;
    }

    void displayInitTDLF( void ){
        printf("hello display");
        SSD1306_SetFont( &I2CDisplay, &Font_droid_sans_mono_7x13 );
        SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_North, "Technologies", SSD_COLOR_WHITE );
        SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_Center, "de la fete", SSD_COLOR_WHITE );
        SSD1306_SetVFlip( &I2CDisplay, 1 ); 
        SSD1306_SetHFlip( &I2CDisplay, 1 ); //void SSD1306_SetHFlip( struct SSD1306_Device* DeviceHandle, bool On );
        SSD1306_Update( &I2CDisplay ); 
    } 

    void displayLink( char *myStr_IP ){
        SSD1306_Clear( &I2CDisplay, SSD_COLOR_BLACK );
        SSD1306_SetFont( &I2CDisplay, &Font_droid_sans_mono_7x13 );
        SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_North, myStr_IP, SSD_COLOR_WHITE ); 
        SSD1306_SetFont( &I2CDisplay, &Font_droid_sans_mono_13x24 );
        SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_Center, "Link!", SSD_COLOR_WHITE );
        SSD1306_Update( &I2CDisplay ); 
    }

    void displayUseESPTouch( void){
          SSD1306_Clear( &I2CDisplay, SSD_COLOR_BLACK );
          SSD1306_SetFont( &I2CDisplay, &Font_droid_sans_mono_7x13 );
          SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_North, "Use", SSD_COLOR_WHITE );
          SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_Center, "ESPTouch", SSD_COLOR_WHITE );
          SSD1306_Update( &I2CDisplay );  
    }

    void displaySetInvert(int setValue){
        SSD1306_SetInverted( &I2CDisplay, setValue);   
    }

    void displaySession(int nmbrClients, char *phases, char *tmpOHbuf, char *current_phase_step){
        snprintf(top, 20, "clients:%i  %s", nmbrClients, phases);
        SSD1306_Clear( &I2CDisplay, SSD_COLOR_BLACK );
        SSD1306_SetFont( &I2CDisplay, &Font_droid_sans_mono_7x13);
        SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_North, top, SSD_COLOR_WHITE ); 
        SSD1306_SetFont( &I2CDisplay, &Font_droid_sans_mono_13x24); // &Font_droid_sans_mono_13x24 // &Font_droid_sans_fallback_15x17
        SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_Center, tmpOHbuf, SSD_COLOR_WHITE );
        SSD1306_FontDrawAnchoredString( &I2CDisplay, TextAnchor_South, current_phase_step, SSD_COLOR_WHITE );
        SSD1306_Update( &I2CDisplay ); 
    }

} // extern "C"
