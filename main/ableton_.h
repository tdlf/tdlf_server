#ifndef ABLETON_H
#define ABLETON_H

#include <cstdint>
extern char zeDrums [];

void startStopChanged(bool state);
void initTickTask();
void tickTask();
void setBroadcastSocket();
void sendMessage(const char* address, const char* format, uint8_t typeValue, const char* textValue, float floatValue, int intValue);
void sendBlobMessage(const char* address, const char* blobData, uint32_t blobSize);

#endif