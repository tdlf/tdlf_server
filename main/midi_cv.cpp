#include "midi_cv.h"
#include "driver/gpio.h"
#include "driver/uart.h"
#include "hal/gpio_types.h"
#include "driver/ledc.h"
#include "hal/ledc_types.h"

// CV Outputs //
int CV_TIMING_CLOCK = 0; 

// Serial midi
#define ECHO_TEST_RTS (UART_PIN_NO_CHANGE)
#define ECHO_TEST_CTS (UART_PIN_NO_CHANGE)
#define ECHO_TEST_TXD  (GPIO_NUM_26) // was TTGO touch  pin 4 (GPIO_NUM_17) // 13 // change for GPIO_NUM_17 // GPIO_NUM_26
#define ECHO_TEST_RXD  (GPIO_NUM_19) 
#define BUF_SIZE (1024)
#define MIDI_TIMING_CLOCK 0xF8
#define MIDI_NOTE_OFF 0x80 // 10000000 // 128


//////// CC messages config //////////
// Load MIDI CC config from NVS if it exists and turn this off is a cfg exists //
// We might not need to do much config of CC messages as the sensor sends it's own Channel and CC values
bool need2configCC = true; // Keep an eye out for sensor messages, if so, start the config of midi CC messages
bool configCC = false; // maybe only one of these two is needed !
bool configCCChannel = false;
int CCChannel1 = 1;
int prevCCmessage = 0;
int compteur = 0;
int CCmessages[9] = {0};
// char MIDI_CONTROL_CHANGE_CH[] = {0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF}; // send control change on channel 1-16, 
bool configCCmessage = false;
// int CCmessage = 0;
int CCmessage1 = 0;
char MIDI_CONTROL_NUMBER[] = {0x36,0x37}; // stutter time (volca beats), stutter depth (volca beats) // find another way to cfg this


// penser à un système qui réarrange les notes selon les gammes, on a huit voix...
// est-ce que l'utilisateur peut transposer d'octave...sans doute


#define MIDI_SONG_POSITION_POINTER 0xF2

void periodic_timer_callback(void* arg) {
    char zedata[] = { MIDI_TIMING_CLOCK };
    // ESP_LOGI(LINK_TAG, "MIDI_TIMING_CLOCK");
    uart_write_bytes(UART_NUM_1, zedata, 1);

    // are we in play mode?
    if(CV_TIMING_CLOCK % 6 == 0 ){ // faire modulo 6 pour arriver à 4 ticks par beat // 24 times per second
      gpio_set_level(GPIO_NUM_5, 1); // CV Clock Out 
      // ESP_LOGI(CV_TAG, "CLOCK 1");
      CV_TIMING_CLOCK = 0;
    } else {
      gpio_set_level(GPIO_NUM_5, 0); 
      // ESP_LOGI(CV_TAG, "CLOCK 0");
    }  
    CV_TIMING_CLOCK = CV_TIMING_CLOCK+1;
    // ESP_LOGI(CV_TAG, "CV_TIMING_CLOCK, %i", CV_TIMING_CLOCK);
}

void initMidiSerial() {
  uart_config_t uart_config = {
    .baud_rate = 31250, // midi speed
    .data_bits = UART_DATA_8_BITS,
    .parity    = UART_PARITY_DISABLE,
    .stop_bits = UART_STOP_BITS_1,
    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    .rx_flow_ctrl_thresh = 122,
  };
  uart_param_config(UART_NUM_1, &uart_config);
  uart_set_pin(UART_NUM_1, ECHO_TEST_TXD, ECHO_TEST_RXD, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
  uart_driver_install(UART_NUM_1, BUF_SIZE * 2, 0, 0, NULL, 0);
 
}

extern "C" {

    //// LEDC //// PWM //// CV Pitch //// 5 (DAC_B clock) 23 (DAC_C) 18 (DAC_D) 
    #define LEDC_HS_TIMER          LEDC_TIMER_0
    #define LEDC_HS_MODE           LEDC_HIGH_SPEED_MODE
    #define LEDC_HS_CH0_GPIO       (23)
    #define LEDC_HS_CH0_CHANNEL    LEDC_CHANNEL_0
    #define LEDC_DUTY              (4095) // Set duty to 50%. ((2 ** 13) - 1) * 50% = 4095
    // #define LEDC_LS_TIMER          LEDC_TIMER_1
    // #define LEDC_LS_MODE           LEDC_LOW_SPEED_MODE

    ledc_timer_config_t ledc_timer = {
        .speed_mode = LEDC_HS_MODE,             // Timer mode
        .duty_resolution = LEDC_TIMER_13_BIT,   // Resolution of PWM duty
        .timer_num = LEDC_HS_TIMER,             // Timer index
        .freq_hz = 5000,                        // Set output frequency at 5 kHz
        .clk_cfg = LEDC_AUTO_CLK                // Auto select the source clock
      };

    ledc_channel_config_t ledc_channel = {
        .gpio_num   = LEDC_HS_CH0_GPIO,
        .speed_mode = LEDC_HS_MODE,
        .channel    = LEDC_HS_CH0_CHANNEL,
        .timer_sel  = LEDC_HS_TIMER,
        .duty       = 0,
        .hpoint     = 0     
      };
 
  void initLedc(){
    ledc_timer_config(&ledc_timer);
    ledc_channel_config(&ledc_channel);
    ledc_fade_func_install(0);
  }
} // End Extern "C" 

