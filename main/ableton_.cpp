#include "display.h"
#include "socket_server.cpp"

#define BROADCAST_UDP_PORT 3337
#define BROADCAST_IP "255.255.255.255"
char udp_packet[48] = "Technologies de la fête";
struct sockaddr_in broadcast_dest_addr;
int broadcastSock;

// AbletonLink utility functions
#include "esp_log.h"
#include "tdlf.h"
#include "ableton_.h"

static const char *LINK_TAG = "Link";
static const char *MIDI_TAG = "Midi";
static const char *TAP_TAG = "Tap";
//static const char *TAG = "Tag";

#define MIDI_START 0xFA // 11111010 // 250
#define MIDI_STOP 0xFC // 11111100 // 252
int MIDI_NOTES_DELAYED_OFF[16] = {0};
char MIDI_NOTE_ON_CH[] = {0x90,0x91,0x92,0x92,0x93,0x94,0x95,0x96,0x97,0x98,0x99,0x9A,0x9B,0x9C,0x9D,0x9E,0x9F}; // note on, channel 10, note on, channel 0 // ajouter d'autres séries
char MIDI_NOTES[16]; // keep notes in memory along with interval at which to trigger the note off message
#define MIDI_NOTE_VEL_OFF 0x00 // 0 // note off
#define MIDI_NOTE_VEL 0x64 // 1100100 // 100 // note on,  // data

// uint8_t oldStepper;

char zeDrums [] = {0x24,0x26,0x2B,0x32,0x2A,0x2E,0x27,0x4B,0x43,0x31}; // midi drum notes in hexadecimal format
char zeDark[] = {0x3D,0x3F,0x40,0x41,0x42,0x44,0x46,0x47}; // A#(70)(0x46), B(71)(0x47), C#(61)(0x3D), D#(63)(0x3F), E(64)(0x40), F(65)(0x41), F#(66)(0x42), G#(68)(0x44)

// octave : C |C# D |D# E F |F# G|G# A|A# B 

/*
0x24 // 36 // C2 //  Kick
0x26 // 38 // D2 //  Snare
0x2B // 43 // G2 //  Low tom
0x32 // 50 // D3 //  Hi Tom 
0x2A // 42 // F#2 // Closed Hat 
0x2E // 46 // A#2 // Open Hat
0x27 // 39 // D#2 // Clap 
0x4B // 75 // D#5 // Claves 
0x43 // 67 // G4 //  Agogo 
0x31 // 49 // C#3 // Crash
*/

char current_phase_step[20]; // 4
int CV_TRIGGER_OFF[16] = {0};

void IRAM_ATTR timer_group0_isr(void* userParam)
{
  static BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  TIMERG0.int_clr_timers.t0 = 1;
  TIMERG0.hw_timer[0].config.alarm_en = 1;

  xSemaphoreGiveFromISR(userParam, &xHigherPriorityTaskWoken);
  if (xHigherPriorityTaskWoken)
  {
    portYIELD_FROM_ISR();
  }
}

void timerGroup0Init(int timerPeriodUS, void* userParam)
{
  timer_config_t config = {.alarm_en = TIMER_ALARM_EN,
    .counter_en = TIMER_PAUSE,
    .intr_type = TIMER_INTR_LEVEL,
    .counter_dir = TIMER_COUNT_UP,
    .auto_reload = TIMER_AUTORELOAD_EN,
    .divider = 80};

  timer_init(TIMER_GROUP_0, TIMER_0, &config);
  timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0);
  timer_set_alarm_value(TIMER_GROUP_0, TIMER_0, timerPeriodUS);
  timer_enable_intr(TIMER_GROUP_0, TIMER_0);
  timer_isr_register(TIMER_GROUP_0, TIMER_0, &timer_group0_isr, userParam, 0, nullptr);

  timer_start(TIMER_GROUP_0, TIMER_0);
}

// Ableton callbacks
void tempoChanged(double tempo) {
    ESP_LOGI(LINK_TAG, "tempochanged");
    double midiClockMicroSecond = ((60000 / tempo) / 24) * 1000;
    esp_timer_handle_t periodic_timer_handle = (esp_timer_handle_t) periodic_timer;
    ESP_ERROR_CHECK(esp_timer_stop(periodic_timer_handle));
    ESP_ERROR_CHECK(esp_timer_start_periodic(periodic_timer_handle, midiClockMicroSecond));
    sendMessage("/abletonLink/bpm", "f", 1, "null", tempo, 0);
}

void startStopChanged(bool state) {
  // received as soon as sent, we can get the state of 'isPlaying' and use that
  // need to wait for phase to be 0 (and deal with latency...)
  startStopCB = state;
  changeLink = true;
  sendMessage("/abletonLink/playState", "i", 2, "null", 0, state);
  //ESP_LOGI(LINK_TAG, "startstopCB is : %i", state);
  //ESP_LOGI(LINK_TAG, "changeLink is : %i", state);
}

void delayer(int64_t del) {
  delset = esp_timer_get_time()+del;
  ESP_LOGI(TAP_TAG, "delaying until : %lli", delset); 
}

void setBroadcastSocket(){

  broadcastSock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
  if (broadcastSock < 0) {
    printf("Failed to create socket!\n");
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    // Enable broadcast option for the socket
    int broadcast_enable = 1;
    if (setsockopt(broadcastSock, SOL_SOCKET, SO_BROADCAST, &broadcast_enable, sizeof(broadcast_enable)) < 0) {
      printf("Failed to set socket option for broadcast!\n");
      close(broadcastSock);
      vTaskDelay(1000 / portTICK_PERIOD_MS);
      }
    broadcast_dest_addr.sin_addr.s_addr = inet_addr(BROADCAST_IP);
    broadcast_dest_addr.sin_family = AF_INET;
    broadcast_dest_addr.sin_port = htons(BROADCAST_UDP_PORT);

    // This broadcasts udp_packet to the clients, can be used to send out the saved sequences instead. 
    int err = sendto(broadcastSock, udp_packet, strlen(udp_packet), 0, (struct sockaddr *)&broadcast_dest_addr, sizeof(broadcast_dest_addr));
      if (err < 0) {
        printf("Error occurred during sending UDP broadcast!\n");
      } else {
        printf("UDP broadcast message sent!\n");
      }
}

void sendMessage(const char* address, const char* format, uint8_t typeValue, const char* textValue, float floatValue, int intValue){

    char monBuffer[68];
    int maLen = strlen(monBuffer);

    if(typeValue == 0){  
      maLen = tosc_writeMessage(
      monBuffer, 
      sizeof(monBuffer),
      address, // the address
      format,   // the format; 'f':32-bit float, 's':ascii string, 'i':32-bit integer
      textValue 
      ); 
      ESP_LOGE(TAG, "textValue: %s\n", textValue);
      } else if (typeValue == 1){
        maLen = tosc_writeMessage(
        monBuffer, 
        sizeof(monBuffer),
        address, // the address
        format,   // the format; 'f':32-bit float, 's':ascii string, 'i':32-bit integer
        floatValue 
        ); 
        } else if (typeValue == 2){
          maLen = tosc_writeMessage(
          monBuffer, 
          sizeof(monBuffer),
          address, // the address
          format,   // the format; 'f':32-bit float, 's':ascii string, 'i':32-bit integer
          intValue 
          ); 
          //ESP_LOGE(TAG, "intValue : %i", intValue);
        }
   
  int err = sendto(broadcastSock, monBuffer, maLen, 0, (struct sockaddr *)&broadcast_dest_addr, sizeof(broadcast_dest_addr));
  
  if (err < 0) {
    ESP_LOGE(TAG, "Error sending sendMessage UDP : %d", errno);
    printf("err = %i\n", err);
    printf(esp_err_to_name(err));
    printf("\n");
    }
}

void sendBlobMessage(const char* address, const char* blobData, uint32_t blobSize){

    char buffer[1024]; // Buffer for the OSC message 
    uint32_t len = tosc_writeMessage(buffer, (uint32_t)1024, address, "b", (uint32_t)blobSize, blobData); // Initialize the OSC message

    // printf("len = %i\n", len);
    // printf("blobSize = %i\n", blobSize);

    int err = sendto(broadcastSock, buffer, len, 0, (struct sockaddr *)&broadcast_dest_addr, sizeof(broadcast_dest_addr));

    if (err < 0) {
      ESP_LOGE(TAG, "Error sending UDP BLOB message : %d", errno);
      printf("err = %i\n", err);
      printf(esp_err_to_name(err));
      printf("\n");
      }
    //printf("Blob sent");
}


// ableton task
void tickTask(void* userParam){
  // connect link
  ableton::Link link(120.0f);
  link.enable(true);
  link.enableStartStopSync(true); // if not no callback for start/stop

  // callbacks
  link.setTempoCallback(tempoChanged);
  link.setStartStopCallback(startStopChanged);
  //setBroadcastSocket();

  // phase
  while (true)
  { 
    xSemaphoreTake((QueueHandle_t)userParam, portMAX_DELAY);
    const auto state = link.captureAudioSessionState(); 
    isPlaying = state.isPlaying();
    //ESP_LOGI(LINK_TAG, "isPlaying : , %i", isPlaying);  
    //const auto phase = state.phaseAtTime(link.clock().micros(), 1); 
    //ESP_LOGI(LINK_TAG, "tempoINC : %i", tempoINC);
    //ESP_LOGI(LINK_TAG, "tempoDEC : %i", tempoDEC);
    //ESP_LOGI(LINK_TAG, "saveSeqConf : %i", saveSeqConf);
    //ESP_LOGI(LINK_TAG, "toTapped : %i", toTapped );

    if ( saveSeqConf == true ) {
      // writeSeqNVS();
    }

    if ( loadSeqConf == true ) {
      // loadSeqNVS();
    }

    if ( tempoINC == true ) {
      const auto tempo = state.tempo(); // quelle est la valeur de tempo?
      newBPM = tempo + 1;
      ESP_LOGI(LINK_TAG, "BPM changed %i", int(newBPM));
      oldBPM = newBPM; // store this to avoid spurious calls
      auto mySession = link.captureAppSessionState();
      const auto timez = link.clock().micros();
      mySession.setTempo(newBPM,timez); // setTempo()'s second arg format is : const std::chrono::microseconds atTime
      link.commitAppSessionState(mySession); // le problème est que l'instruction de changer le tempo nous revient
      tempoINC = false;
      saveBPM = false; // as changing the BPM here implies not wanting to tap the tempo in
      myNoteDuration = ((60/(tempo*4))*1000)*noteDuration; // calculate noteDuration as a function of BPM // 60 BPM * 4 steps per beat = 240 steps per minute // 60 seconds / 240 steps = 0,25 secs or 250 milliseconds per step // ((60 seconds / (BPM * 4 steps per beat))*1000 ms)*noteDuration
      //ESP_LOGI(LINK_TAG, "tempoINC : %i", tempoINC);
    }

    if ( tempoDEC == true ) {
      const auto tempo = state.tempo(); // quelle est la valeur de tempo?
      newBPM = tempo - 1;
      ESP_LOGI(LINK_TAG, "BPM changed %i", int(newBPM));
      oldBPM = newBPM; // store this to avoid spurious calls
      auto mySession = link.captureAppSessionState();
      const auto timez = link.clock().micros();
      mySession.setTempo(newBPM,timez); // setTempo()'s second arg format is : const std::chrono::microseconds atTime
      link.commitAppSessionState(mySession);

      tempoDEC = false;
      saveBPM = false;
      myNoteDuration = ((60/(tempo*4))*1000)*noteDuration; // calculate noteDuration as a function of BPM // 60 BPM * 4 steps per beat = 240 steps per minute // 60 seconds / 240 steps = 0,25 secs or 250 milliseconds per step // ((60 seconds / (BPM * 4 steps per beat))*1000 ms)*noteDuration
    }

    if ( changePiton && startStopState != startStopCB ){ // if local state is different to the SessionState then send to the session state 
        auto mySession = link.captureAppSessionState();
        const auto timez = link.clock().micros();
        mySession.setIsPlaying(startStopState, timez);
        link.commitAppSessionState(mySession);
      }
      
    if ( changeLink && startStopState != startStopCB ){ // if CB state is different to the local startStopState then resync the latter to the former
       startStopState = startStopCB; // resync 
      }

    if( toTapped == true ){
      tapped = esp_timer_get_time()/1000;
      tapInterval = tapped - lastTapTime;
      // ESP_LOGI(TAP_TAG, "Time at tap: %ld ", tapped);
      ESP_LOGI(TAP_TAG, "Time at tap: %i ", tapped);
      ESP_LOGI(TAP_TAG, "Interval between taps n: %i ", tapInterval );
      if(tapInterval > maximumTapInterval){ // reset tapCounter to zero
        foisTapped = 0;
      }

      if(minimumTapInterval < tapInterval && tapInterval < maximumTapInterval){ // only add a tap if we are within the tempo bounds
        foisTapped = foisTapped + 1;
        ESP_LOGI(TAP_TAG, "foisTapped %i", foisTapped);
        // add a tapInterval to the sliding values array
        tapArray[foisTapped%5] = tapInterval;
      }
      
      if(foisTapped > 5){  // calculate BPM from tapInterval
        for(int i = 0; i<6; i++){
          tapTotal = tapArray[i] + tapTotal;
          ESP_LOGI(TAP_TAG, "tapped interval at %i, %i", i, tapArray[i]);
        }
        tapTotal = tapTotal / 6;
        tappedBPM = 60L * 1000 / tapTotal;
        ESP_LOGI(TAP_TAG, "tapped BPM %i", tappedBPM);
        tapTotal = 0;
        saveBPM = true;
        saveDelay = true; // appeler une fonction pour remettre les variables à false après un moment
        delayer(3000000);
      }

      lastTapTime = tapped;
      toTapped = false;
    }

    if ( esp_timer_get_time() > delset ){ // Resets actions states
     saveBPM = false;
     saveSeq = false; 
     seqSaved = false;
     loadSeq = false;
     seqLoaded = false;
   }

    if ( saveBPM == true && tapeArch == true ) {
      auto mySession = link.captureAppSessionState();
      const auto timez = link.clock().micros();
      mySession.setTempo(tappedBPM,timez); // setTempo()'s second arg format is : const std::chrono::microseconds atTime
      link.commitAppSessionState(mySession);
      saveBPM = false;
      tapeArch = false;
    }

    //ESP_LOGI(LINK_TAG, "POST startStopState is :  %i", startStopState);  
    //ESP_LOGI(LINK_TAG, "POST startStopCB is :  %i", startStopCB); 
   
    curr_beat_time = state.beatAtTime(link.clock().micros(), 4);
    const double curr_phase = fmod(curr_beat_time, 4);  // const double


    if ( curr_beat_time > prev_beat_time ) {
      
      const double prev_phase = fmod(prev_beat_time, 4);
      const double prev_step = floor(prev_phase * 4);
      const double curr_step = floor(curr_phase * 4);
      
      //ESP_LOGI(LINK_TAG, "current step : %f", curr_step); 
      //ESP_LOGI(LINK_TAG, "prev step : %f", prev_step); 
     
      if (abs(prev_phase - curr_phase) > 4 / 2 || prev_step != curr_step) { // quantum divisé par 2

        if( ( curr_step == 0 && changePiton ) || ( curr_step == 0 && changeLink ) ) { // start / stop implementation
            
          if(startStopCB) { 
            char zedata[] = { MIDI_START };
            uart_write_bytes(UART_NUM_1, zedata, 1);
            changeLink = false;
            changePiton = false;
            step = 0; // reset step count
            } else { // on jouait et on arrête // changer pout s'arrêter immédiatement après un stop 
              char zedata[] = { MIDI_STOP };
              uart_write_bytes(UART_NUM_1, zedata, 1);
              changeLink = false;
              changePiton = false;
              }
            }
  
        if (step == 64){ // max steps = 64
          step = 0;
          }

        if(startStopCB){
          displaySetInvert(1);
          }
          
        if(!startStopCB){
          displaySetInvert(0); 
          }

        const int halo_welt = int(curr_phase);

        const int tmpOH = (int) round( state.tempo() );
        // ESP_LOGI(LINK_TAG, "tempo %i", tmpOH); 

        char phases[5] = "TDLF";

        char tmpOHbuf[20];

        if ( seqSaved == true ) {
          snprintf(tmpOHbuf, 20 , "Sequence"); 
          snprintf(current_phase_step, 20, "Saved   ");
        }

        else if ( saveSeq == true ) {
          snprintf(tmpOHbuf, 20 , "Sequence"); 
          snprintf(current_phase_step, 20, "Save?   ");
        }

        else if ( seqLoaded == true ) {
          snprintf(tmpOHbuf, 20 , "Sequence"); 
          snprintf(current_phase_step, 20, "%i loaded", selectedSeq);
        }

        else if ( loadSeq == true ) {
          snprintf(tmpOHbuf, 20 , "Sequence"); 
          snprintf(current_phase_step, 20, "Load:+%i-?", selectedSeq);
        }

        else if ( saveSeq == true ) {
          snprintf(tmpOHbuf, 20 , "Sequence"); 
          snprintf(current_phase_step, 20, "Save:+%i-?", selectedSeq);
        }
        
        else if (saveBPM == false){
          snprintf(tmpOHbuf, 20 , "%i BPM", tmpOH );     /////// display BPM + Phase + Step /////////

          if (step < 10){
            snprintf(current_phase_step, 20, " 0%i STP", step);
          }
          else {
            snprintf(current_phase_step, 20, " %i STP", step);
          }
        }
        
        else if (saveBPM == true){ // we have a tapped BPM ready to switch?
          snprintf(tmpOHbuf, 20 , "%i BPM?", tappedBPM ); 
          
          if (step < 10){
            snprintf(current_phase_step, 20, " 0%i STP", step);
          }
          else {
            snprintf(current_phase_step, 20, " %i STP", step);
          }
        }

      switch (halo_welt)
      {
        case 0:
        strcpy(phases, "X000");
        break;
        case 1:
        strcpy(phases, "XX00");
        break;
        case 2:
        strcpy(phases, "XXX0"); 
        break;
        case 3:
        strcpy(phases, "XXXX");
        break;
        default:
        ESP_LOGI(LINK_TAG, "phases not assigned correctly"); 
      }
        displaySession(nmbrClients, phases, tmpOHbuf, current_phase_step);

        if (startStopCB){ // isPlaying and did we send that note out?
          
          //// Send step via OSC here ////
          int stepper = step % 64; // The step value to send to the sequencer client 
          //ESP_LOGI(LINK_TAG, "stepper : %i", stepper);

          //if (stepper > oldStepper){ // only send this out when the step changes
          sendMessage("/abletonLink/curBeat","i",2,"null",0,stepper);
            
    // shut off the midi notes that need to be 
    int monTemps = int(esp_timer_get_time()/1000);
    //ESP_LOGI(MIDI_TAG, "Mon Temps, %i", monTemps);
      //for( int i = 0; i < 8; i++ ) {
        //if( MIDI_NOTES_DELAYED_OFF[i] > 0 && MIDI_NOTES_DELAYED_OFF[i] < monTemps ) {

      if( CV_TRIGGER_OFF[0] > 0 && CV_TRIGGER_OFF[0] < monTemps ) {
          // gpio_set_level(GPIO_NUM_18, 0); // DAC_D // CV Trigger
          // gpio_set_level(CV_5, 0); // DAC_B // CV Clock Out
          // gpio_set_level(CV_23, 0); // DAC_C // CV Pitch
        }

      if( MIDI_NOTES_DELAYED_OFF[0] > 0 && MIDI_NOTES_DELAYED_OFF[0] < monTemps ) {
        // ESP_LOGI(MIDI_TAG, "Should attempt to turn this off : %i", i);
        //char zedata0[] = {MIDI_NOTE_OFF};
        //uart_write_bytes(UART_NUM_1,zedata0,1); // note off before anything
          
        char zedata1[] = { MIDI_NOTE_ON_CH[1] }; // défini comme midi channel 1 
        uart_write_bytes(UART_NUM_1, zedata1, 1); //uart_write_bytes(UART_NUM_1, "0x90", 1); // note on channel 0
        char zedata2[] = {MIDI_NOTES[0]}; // tableau de valeurs de notes hexadécimales 
        //char zedata2[] = {MIDI_NOTES[i]}; // tableau de valeurs de notes hexadécimales 
        uart_write_bytes(UART_NUM_1, zedata2, 1); 
        char zedata3[] = {MIDI_NOTE_VEL_OFF};
        uart_write_bytes(UART_NUM_1,zedata3, 1); // velocité à 0
      }
    //} // end midi note off
           
        myBar = devices[curVoice].bar; // We're working on 16 steps so myBar is always '0'    
        //ESP_LOGI(LINK_TAG, "myBar : %i", myBar);

        dubStep = step%(myBar*16+16); // modulo 16 // 32 // 48 // 64

        // ESP_LOGI(LINK_TAG, "dubStep : %i", dubStep);

        for (int i = 0; i <= 7; i++){
          // ESP_LOGI(LINK_TAG, "mute : %i", steps[i][dubStep].mute);
          if (devices[i].steps[dubStep].on == 1 && devices[i].mute != 1){

            if (devices[i].chan == 10){ // are we playing drumz // solenoids ?
              
              char zedata1[] = { MIDI_NOTE_ON_CH[devices[i].chan] }; // défini comme channel 10(drums), ou channel 1(synth base) pour l'instant mais dois pouvoir changer
              uart_write_bytes(UART_NUM_1, zedata1, 1); // this function will return after copying all the data to tx ring buffer, UART ISR will then move data from the ring buffer to TX FIFO gradually.
              char zedata2[] = {zeDrums[i]};  
              uart_write_bytes(UART_NUM_1, zedata2, 1); // tableau de valeurs de notes hexadécimales 
              char zedata3[] = { MIDI_NOTE_VEL };
              uart_write_bytes(UART_NUM_1, zedata3, 1); // vélocité
                
              //gpio_set_level(GPIO_NUM_18,1); // DAC_D (CV_18) get that note out !    
              //CV_TRIGGER_OFF[0] = int((esp_timer_get_time()/1000)+(myNoteDuration/64)); // set trigger duration
            
            } else if (devices[i].chan == 5){ // synth, {0x99,0x90};

              char zedata1[] = { MIDI_NOTE_ON_CH[devices[i].chan] }; // défini comme midi channel channel 0 
              // ESP_LOGI(MIDI_TAG, "MIDI_NOTE_ON_CH, %i", MIDI_NOTE_ON_CH[channel]);
              uart_write_bytes(UART_NUM_1, zedata1, 1); 

              char zedata2[] = {zeDark[i]}; // tableau de valeurs de notes hexadécimales 
              uart_write_bytes(UART_NUM_1, zedata2, 1); 

              char zedata3[] = { MIDI_NOTE_VEL };
              uart_write_bytes(UART_NUM_1, zedata3, 1); // vélocité
                  
              //const auto tempo = state.tempo(); // quelle est la valeur de tempo?
              //myNoteDuration = ((60/(tempo*4))*1000)*noteDuration; // calculate noteDuration as a function of BPM // 60 BPM * 4 steps per beat = 240 steps per minute // 60 seconds / 240 steps = 0,25 secs or 250 milliseconds per step // ((60 seconds / (BPM * 4 steps per beat))*1000 ms)*noteDuration
              // check if there is a value in the array and populate it or just add the note
              //int posArray = step%8;
              //ESP_LOGI(MIDI_TAG, "posArray, %i", posArray);
              //MIDI_NOTES[posArray] = zeDark[i];
              //MIDI_NOTES_DELAYED_OFF[posArray] = int((esp_timer_get_time()/1000)+myNoteDuration); // duration

              MIDI_NOTES[0] = zeDark[0];
              MIDI_NOTES_DELAYED_OFF[0] = int((esp_timer_get_time()/1000)+myNoteDuration); // duration

              ESP_LOGI(MIDI_TAG, "MIDI NOTE, %i", MIDI_NOTES[0]);
              ESP_LOGI(MIDI_TAG, " "); // new line
              // ESP_LOGI(MIDI_TAG, "MIDI TIME, %i", MIDI_NOTES_DELAYED_OFF[0]);

              } else {

                ESP_LOGI(MIDI_TAG, "Midi channel other than 10 or 5"); // ajouter d'autres gammes (scales)
                
              } 
                
              //char zedata3[] = { MIDI_NOTE_VEL };
              //uart_write_bytes(UART_NUM_1, zedata3, 1); // vélocité
              uart_write_bytes(UART_NUM_1, "0", 1); // ??
              }       
          } // end of for loop that iterates through the 8 possible sequences
      }

      if(isPlaying){
        // ESP_LOGI(LINK_TAG, "We should see this step if we're playing : step : %d", step); 
        step++; // might be off to add '1' right away
      }

    }

    } // fin de if curr_beat_time is > prev_beat_time

    prev_beat_time = curr_beat_time;
    portYIELD();
  }

} // fin de tick task

void initTickTask(){
  SemaphoreHandle_t tickSemphr = xSemaphoreCreateBinary();
  timerGroup0Init(1000, tickSemphr);
  xTaskCreate(tickTask, "tick", 8192, tickSemphr, 1, nullptr); // The abletonLink task
}