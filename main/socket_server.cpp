#include "esp_log.h"
#include "midi_cv.h"
#include "socket_server.h"
#include "ableton_.h"
#include "tdlf.h"
// #include <cstdint>
#include <cstdio>
#include <cstring>
#include <stdio.h>
#include "freertos/task.h"
#include "lwip/sockets.h"
#include "tinyosc.h"
#include "string.h"

#define PORT 7002
#define MAX_PACKET_SIZE 1024

bool live = 0;

static const char *TAG = "UDP Server";

struct sockaddr_in clientIPAddresses[8];

bool client_exists(struct sockaddr_in *client) {
    for (int i = 0; i < nmbrClients; ++i) {
        if (clientIPAddresses[i].sin_addr.s_addr == client->sin_addr.s_addr) {
            return true;
        }
    }
    return false;
}

void add_client_address(struct sockaddr_in *client) {
    if (!client_exists(client)) {
        if (nmbrClients < 8) {
            clientIPAddresses[nmbrClients] = *client;
            nmbrClients++;
            ESP_LOGI(TAG, "Added new client address: %s", inet_ntoa(client->sin_addr));
        } else {
            ESP_LOGW(TAG, "Client address array is full. Cannot add new client address.");
        }
    } else {
        ESP_LOGI(TAG, "Client address %s already exists in the array.", inet_ntoa(client->sin_addr));
    }
}

void udp_server_task(void *pvParameters) {
    
    int sockfd;
    struct sockaddr_in server_addr, client_addr;
    socklen_t client_addr_len = sizeof(client_addr);
    char rx_buffer[MAX_PACKET_SIZE];
    int bytes_received;

    // Create a UDP socket
    sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sockfd < 0) {
        ESP_LOGE(TAG, "Failed to create socket");
        vTaskDelete(NULL);
        return;
    }

    // Bind the socket to the port
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(PORT);
    if (bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        ESP_LOGE(TAG, "Socket bind failed");
        close(sockfd);
        vTaskDelete(NULL);
        return;
    }

    ESP_LOGE(TAG, "UDP Echo Server started on port %d", PORT);

    while (1) {
        // Receive data from a client
        bytes_received = recvfrom(sockfd, rx_buffer, sizeof(rx_buffer), 0, (struct sockaddr *)&client_addr, &client_addr_len);
        if (bytes_received < 0) {
            ESP_LOGE(TAG, "recvfrom failed");
            continue;
        } 

        printf("IP Address : %s\n", inet_ntoa(client_addr.sin_addr));  
        // tosc_printOscBuffer(rx_buffer, sizeof(rx_buffer)); // FROM TINYOSC 
        tosc_message osc; // declare the TinyOSC structure

        int errOsc = tosc_parseMessage(&osc, rx_buffer, sizeof(rx_buffer));
        if(errOsc<0){
          printf("Could not parse osc message");
        }

        // printf("format : %s\n", tosc_getFormat(&osc));  
        printf("address : %s\n",tosc_getAddress(&osc)); // the OSC address string
      
      if (strcmp(tosc_getAddress(&osc), "/abletonLink/curBeat") == 0 ) {
        ESP_LOGI(TAG, " We have a beat of format : %c", osc.format[0]);
        } else if (strcmp(tosc_getAddress(&osc), "/abletonLink/bpm") == 0) {
          ESP_LOGI(TAG, " We have a new bpm of format : %c", osc.format[0]);
        } else if (strncmp(tosc_getAddress(&osc), "/tdlf/live/",11) == 0) {

          curVoice = atoi(tosc_getAddress(&osc) + strlen("/tdlf/device/"));
          printf("live curVoice number : %i \n", curVoice);

          int blobSize;
          const char* blobData;
          tosc_getNextBlob(&osc, &blobData, &blobSize);
          memcpy(&devices[curVoice], blobData, sizeof(devices[curVoice]));

          printf("voice : %i\n", devices[curVoice].voice);

          // Hard-coded for drums atm
          char zedata1[] = { 10 }; 
          uart_write_bytes(UART_NUM_1, zedata1, 1); // this function will return after copying all the data to tx ring buffer, UART ISR will then move data from the ring buffer to TX FIFO gradually.
          char zedata2[] = {zeDrums[curVoice]};  
          uart_write_bytes(UART_NUM_1, zedata2, 1); // tableau de valeurs de notes hexadécimales 
          char zedata3[] = { 0x64 };
          uart_write_bytes(UART_NUM_1, zedata3, 1); // vélocité
          uart_write_bytes(UART_NUM_1, "0", 1); // ??

        } else if (strncmp(tosc_getAddress(&osc), "/tdlf/device/",13) == 0) {
          
          curVoice = atoi(tosc_getAddress(&osc) + strlen("/tdlf/device/"));
          // printf("Extracted device number: %d\n", curVoice);
        
          int blobSize;
          const char* blobData;
          tosc_getNextBlob(&osc, &blobData, &blobSize);
          memcpy(&devices[curVoice], blobData, sizeof(devices[curVoice]));

          printf("voice : %i\n", devices[curVoice].voice);
          printf("chan : %i\n", devices[curVoice].chan);
          printf("bar : %i\n", devices[curVoice].bar);
          printf("note : %i\n", devices[curVoice].note);

          for ( int i = 0 ; i < 16 ; i++ ){
            printf("devices[curVoice].steps[%d].on : %d\n", i, devices[curVoice].steps[i].on);
          }

          char address[20]; // Adjust size as needed
          const char* addressBase = "/tdlf/device/";
          snprintf(address, sizeof(address), "%s%d", addressBase, curVoice);

          sendBlobMessage(address, (char *)&devices[curVoice], sizeof(devices[curVoice]));

        } else if (strcmp(tosc_getAddress(&osc), "/tdlf/channel") == 0){

        // } else if ((strcmp(tosc_getAddress(&osc), "/analogin/a") == 0) || strcmp(tosc_getAddress(&osc), "/analogin/b") == 0) {
        
        } else if (strncmp(tosc_getAddress(&osc), "/tdlf/clientIP", 14) == 0) {
          ESP_LOGI(TAG, " We have a new client IP address message, format : %c", osc.format[0]);
          add_client_address(&client_addr); // Check if the ip is in the list, keep track of which address is set to which voice and bar

        } else if ((strcmp(tosc_getAddress(&osc), "/accel/x/value") == 0) || strcmp(tosc_getAddress(&osc), "/accel/y/value") == 0) {
 
          ESP_LOGI(TAG, " We have a new sensor message, format : %c", osc.format[0]);
          unsigned char *m = tosc_getNextMidi(&osc); 
          // printf("%02i, %02i, %02i, %02i\n", m[0], m[1], m[2], m[3]);
          CCChannel = m[0]-1;
          // printf("CCChannel : , %i\n",CCChannel);
          int CCmessage = m[2];
          // printf("CCmessage : , %i\n",CCmessage);
          sensorValue = m[1];
          // printf("sensorValue : , %i\n ",sensorValue);

          char MIDI_CONTROL_CHANGE_CH[] = {0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB9,0xBA,0xBB,0xBC,0xBD,0xBE,0xBF}; // send control change on channel 1-16, 
        
          char zedata01[] = { MIDI_CONTROL_CHANGE_CH[CCChannel] }; // send CC message on midi channel 1 '0xB0'
          uart_write_bytes(UART_NUM_1, zedata01, 1); // this function will return after copying all the data to tx ring buffer, UART ISR will then move data from the ring buffer to TX FIFO gradually.
        
          char zedata02[] = { (char)CCmessage};  //  '0x36' (54) trying for Volca Beats Stutter time
          uart_write_bytes(UART_NUM_1, zedata02, 1); 
        
          char zedata03[] = { (char)sensorValue }; // need to convert sensorValue to hexadecimal! 
          uart_write_bytes(UART_NUM_1, zedata03, 1); 

          if (sensorValue != previousSensorValue){
            // ESP_LOGI(CV_TAG, "CV_PITCH, %i", int(sensorValue));
            ledc_set_duty(ledc_channel.speed_mode, ledc_channel.channel, int(sensorValue)); // CV_PITCH
            ledc_update_duty(ledc_channel.speed_mode, ledc_channel.channel);
            previousSensorValue = sensorValue;
          }
        }
      }

    close(sockfd);
    vTaskDelete(NULL);
}

void initUdpServerTask(){
    xTaskCreate(udp_server_task, "udp_server", 4096, NULL, 5, NULL); // Start socket server
  }
