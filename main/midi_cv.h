#ifndef MIDI_CV_H
#define MIDI_CV_H

#include "hal/ledc_types.h"

void periodic_timer_callback(void* arg);
void initMidiSerial();

extern "C" {
  extern ledc_channel_config_t ledc_channel;
  extern ledc_timer_config_t ledc_timer;
  extern void initLedc();
}

#endif