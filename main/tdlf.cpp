#include "tdlf.h"
#include <cstdint>

device_t devices[8];

float noteDuration; 
int myNoteDuration;
uint8_t myBar = 0; 
uint8_t curVoice; // 8 voices 

int beat = 0; 
int step = 0 ;
int dubStep = 0;
float oldstep;
int currStep = 0;

esp_timer_handle_t periodic_timer;

bool isPlaying = true;
bool changePiton = false; 
bool changeLink = false;
bool tempoINC = false; // si le tempo doit être augmenté
bool tempoDEC = false; // si le tempo doit être réduit
double newBPM; // send to setTempo();
double oldBPM; 
double curr_beat_time;
double prev_beat_time;

bool connektMode = true; // flag pour envoyer l'adresse IP aux clients
char str_ip[16] =  "192.168.0.66"; // send IP to clients !! // stand in ip necessary for memory space?

int nmbrSeq = 0; // the number of sequences in nvs

///////////// TAP TEMPO //////////////
/// from : https://github.com/DieterVDW/arduino-midi-clock/blob/master/MIDI-Clock.ino

int foisTapped = 0;
bool toTapped = false;
int tapped = 0;
int lastTapTime = 0;
int tapInterval = 0;
int tapArray[5] = {0};
int tapTotal = 0;
int bpm;  // BPM in tenths of a BPM!!
int tappedBPM = 0;
int minimumTapInterval = 50;
int maximumTapInterval = 2500;

int CCChannel = 1;
int sensorValue = -42;
int sensorValue1 = 0;
int previousSensorValue = 0;

bool startStopCB = false; // l'état du callback 
bool startStopState = false; // l'état local

int nmbrClients = 0;
int loadedClients = 0;

///////////// INTERACTIONS ///////////
/// save (Touch pad 2)
bool tapeArch = false; // flag for saving
bool saveBPM = false; 
bool saveSeq = false; // nvs save the sequence to start
bool seqSaved = false;
bool saveSeqConf = false;
bool loadSeq = false;
bool seqLoaded = false;
bool seqToLoad = false; // to know when to send the loaded sequence to clients
bool loadSeqConf = false;
bool saveDelay = false; // for when to remove the save options after an interaction
int64_t delset = 0;
int selectedSeq = 0; // user selected sequence to load