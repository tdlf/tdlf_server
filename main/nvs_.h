#ifndef NVS_H
#define NVS_H

#include "esp_err.h"

void initNVS();
void nvsReadCredentials();
esp_err_t save_devices_to_nvs();
esp_err_t load_devices_from_nvs();

#endif