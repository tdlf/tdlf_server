////// tdlf server ///// receive note, midi channel info through UDP + send midi through serial // Save, Load sequences
////// ESP32 Ableton Link node // midi clock // BPM (+ - )// Start/Stop 
////// Smart config NVS enabled to set the wifi credentials from ESPTouch app if no IP is attributed
#include "tdlf.h"
#include "ableton_.h"
#include "hal/gpio_types.h"
#include "midi_cv.h"
#include "nvs_.h"
#include "socket_server.h"
#include "touch.h"
#include "wifi_mdns.h"

#include <stdbool.h>
//#include <stdint.h>
//#include <stdio.h>
//#include <stdlib.h>

#define GPIO_OUTPUT_PIN_SEL  ( (1ULL<<GPIO_NUM_18) | (1ULL<<GPIO_NUM_5) )

extern "C" { 
  void app_main() {

    initNVS();
    initialise_mdns(); 
    initTouch(); // Initialize the touch pads
    initUdpServerTask();
  
    gpio_config_t io_conf = {}; //zero-initialize the config structure.
    io_conf.intr_type = GPIO_INTR_DISABLE;  //disable interrupt
    io_conf.mode = GPIO_MODE_OUTPUT;  //set as output mode
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;// PINS 18,5 for midi //bit mask of the pins that you want to set,e.g.GPIO18/19
    gpio_config(&io_conf);  //configure GPIO with the given settings */

    initMidiSerial();
    // link timer - phase
    initTickTask(); 
    // midi clock
    setBroadcastSocket();
    const esp_timer_create_args_t periodic_timer_args = {
      .callback = &periodic_timer_callback,
      .name = "periodic"
    };
    ESP_ERROR_CHECK(esp_timer_create(&periodic_timer_args, &periodic_timer));
    ESP_ERROR_CHECK(esp_timer_start_periodic(periodic_timer, 20833.333333333)); // 120 bpm by default
    initLedc(); //// LEDC //// PWM //// CV Pitch ////
  }
} 
