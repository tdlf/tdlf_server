#include "nvs_.h"
#include "ableton_.h"
#include "tdlf.h"
#include "socket_server.h"
#include "esp_log.h"
#include "nvs.h"

#define NVS_NAMESPACE "storage"

static const char *NVS_TAG = "NVS";

bool skipNVSRead = false;

// Function to save devices to NVS
esp_err_t save_devices_to_nvs() {
    ESP_LOGI(NVS_TAG, "Saving devices to NVS");
    nvs_handle_t nvs_handle;
    esp_err_t err = nvs_open(NVS_NAMESPACE, NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK) {
        return err;
    }
    
    // Serialize the devices array into a buffer
    size_t buffer_size = sizeof(devices);
    uint8_t *buffer = (uint8_t *)devices;
    // Write buffer to NVS
    err = nvs_set_blob(nvs_handle, "devices", buffer, buffer_size);
    if (err == ESP_OK) {
        err = nvs_commit(nvs_handle);
    }
    nvs_close(nvs_handle);
    ESP_LOGI(NVS_TAG, "Devices saved to NVS");
    return err;
}

esp_err_t load_devices_from_nvs() {
    ESP_LOGI(NVS_TAG, "Loading devices from NVS");
    nvs_handle_t nvs_handle;
    esp_err_t err = nvs_open(NVS_NAMESPACE, NVS_READONLY, &nvs_handle);
    if (err != ESP_OK) {
        return err;
    }

    // Determine the size of the stored blob
    size_t required_size = 0;
    err = nvs_get_blob(nvs_handle, "devices", NULL, &required_size);
    if (err != ESP_OK) {
        nvs_close(nvs_handle);
        return err;
    }

    // Ensure the blob size matches the devices array size
    if (required_size != sizeof(devices)) {
        nvs_close(nvs_handle);
        return ESP_ERR_INVALID_SIZE;
    }

    // Read the blob into the devices array
    err = nvs_get_blob(nvs_handle, "devices", devices, &required_size);
    nvs_close(nvs_handle);
    ESP_LOGI(NVS_TAG, "Devices loaded from NVS");
    
    for ( int i = 0 ; i < 16 ; i++ ){
      printf("devices[curVoice].steps[%d].on : %d\n", i, devices[curVoice].steps[i].on);
      }

    // For each device in the array, send the blob ut as a message
    char address[32]; 
    const char* addressBase = "/tdlf/device/";
    for ( int i = 0 ; i < 8; i++ ){
      snprintf(address, sizeof(address), "%s%d", addressBase, i);
      sendBlobMessage(address, (char *)&devices[curVoice], sizeof(devices[i]));
      vTaskDelay(500 / portTICK_PERIOD_MS);  
    }
    return err;
}

void nvsReadCredentials(){
    nvs_handle wificfg_nvs_handler; // Might need to revert to previous version
    size_t len;
    nvs_open("Wifi", NVS_READWRITE, &wificfg_nvs_handler);

    nvs_get_str(wificfg_nvs_handler, "wifi_ssid", NULL, &len);
    char* lssid = (char*)malloc(len); // (char*)malloc(len) compiles but crashes
    nvs_get_str(wificfg_nvs_handler, "wifi_ssid", lssid, &len);

    nvs_get_str(wificfg_nvs_handler, "wifi_password", NULL, &len);
    char* lpassword = (char*)malloc(len); // (char*)malloc(len) compiles but crashes
    nvs_get_str(wificfg_nvs_handler, "wifi_password", lpassword, &len); // esp_err_tnvs_get_str(nvs_handle_thandle, const char *key, char *out_value, size_t *length)

    nvs_close(wificfg_nvs_handler);
    
    ESP_LOGI(NVS_TAG,"INIT :%s",lssid); 
    ESP_LOGI(NVS_TAG,"INIT :%s",lpassword); 
}

void initNVS(){
//Initialize and read in wifi credentials from NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    
    /// Error check NVS credentials

    esp_err_t err;
	  size_t len;
	  // Open
	  nvs_handle wificfg_nvs_handler;
	  err = nvs_open("Wifi", NVS_READWRITE, &wificfg_nvs_handler);
        
      if (err != ESP_OK) {
          printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
          skipNVSRead = true;
      } else {
	      nvs_get_str(wificfg_nvs_handler, "wifi_ssid", NULL, &len);
        char* lssid = (char*)malloc(len); // (char*)malloc(len) compiles but crashes
        err = nvs_get_str(wificfg_nvs_handler, "wifi_ssid", lssid, &len);
	      switch (err) {
	        case ESP_OK:
	        break;
	        case ESP_ERR_NVS_NOT_FOUND:
	          printf("Key wifi_ssid is not initialized yet!\n");
            skipNVSRead = true;
	          break;
	        default :
	          printf("Error (%s) reading wifi_ssid size!\n", esp_err_to_name(err));
	          skipNVSRead = true;
            break;
	        }
        }
      nvs_close(wificfg_nvs_handler);

    if(skipNVSRead){
      //////// WRITING DUMMY VALUES TO NVS FROM CLEAN SHEET //////
      nvs_handle wificfg_nvs_handler;
      nvs_open("Wifi", NVS_READWRITE, &wificfg_nvs_handler);
      nvs_set_str(wificfg_nvs_handler,"wifi_ssid","tdlf");
      nvs_set_str(wificfg_nvs_handler,"wifi_password","goodluck");
      nvs_commit(wificfg_nvs_handler); 
      nvs_close(wificfg_nvs_handler); 
      ////// END NVS ///// 
    }
  nvsReadCredentials();
}

