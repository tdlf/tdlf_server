#ifndef TDLF_H
#define TDLF_H

#include <ableton/Link.hpp>
#include <cstdint>
#include <driver/timer.h>
#include <nvs_flash.h>
#include "driver/uart.h"

///////// WiFI ///////
#include "esp_wifi.h"
#include "esp_smartconfig.h"
#include "freertos/event_groups.h"
//#include "lwip/inet.h" // inet.pton() 

///////// TOUCH //////
#include "driver/touch_pad.h" 
#include "soc/rtc_periph.h"
#include "soc/sens_periph.h"

// For CV Clock out and Pitch 
#include "driver/ledc.h" 

///// MDNS //////
// #include "mdns.h"//
#include "netdb.h" 

///// OSC ///////
#include <tinyosc.h> 

void initTDLF();

#define MDNS_PORT 5353

///// step /////
typedef struct {
  bool on;          
  uint8_t length;
  uint8_t vel;
  uint8_t ratch;
} steps_t;

typedef struct {
    uint16_t year;  // e.g., 2025
    uint8_t month;  // 1-12
    uint8_t day;    // 1-31
} date_t;

///// device /////
typedef struct {
    char author[20]; 
    date_t date;
    bool train;
    uint8_t voice;
    uint8_t chan;
    uint8_t bar;
    uint8_t note;
    bool mute;
    steps_t steps[64];
} device_t;

extern device_t devices[8];

extern uint8_t myBar;
extern uint8_t curVoice; // 8 voices

extern float noteDuration; 
extern int myNoteDuration;

extern int beat;
extern int step;
extern int dubStep;
extern float oldstep;
extern int currStep;

extern esp_timer_handle_t periodic_timer;

extern bool isPlaying;
extern bool changePiton;
extern bool changeLink;
extern bool tempoINC;
extern bool tempoDEC;
extern double newBPM; // send to setTempo();
extern double oldBPM; 
extern double curr_beat_time;
extern double prev_beat_time;

extern bool connektMode;
extern char str_ip[16]; // The server IP

extern int nmbrSeq; // the number of sequences in nvs

extern int foisTapped;
extern bool toTapped;
extern int tapped;
extern int lastTapTime;
extern int tapInterval;
extern int tapArray[5];
extern int tapTotal;
extern int bpm;  // BPM in tenths of a BPM!!
extern int tappedBPM;
extern int minimumTapInterval;
extern int maximumTapInterval;

extern int CCChannel;
extern int sensorValue;
extern int sensorValue1;
extern int previousSensorValue;

extern bool startStopCB;
extern bool startStopState;

extern int nmbrClients;
extern int loadedClients;

///////////// INTERACTIONS ///////////
/// save (Touch pad 2)
extern bool tapeArch;
extern bool saveBPM;
extern bool saveSeq;
extern bool seqSaved;
extern bool saveSeqConf;
extern bool loadSeq;
extern bool seqLoaded;
extern bool seqLoaded;
extern bool seqLoaded;
extern bool seqToLoad;
extern bool loadSeqConf;
extern bool saveDelay;
extern int64_t delset;
extern int selectedSeq;

#endif
