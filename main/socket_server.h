#ifndef SOCKET_SERVER_H
#define SOCKET_SERVER_H

// ///// step /////
// extern typedef struct {
//   bool on;          
//   uint8_t length;
//   uint8_t vel;
//   uint8_t ratch;
// } steps_t;

// ///// device /////
// extern typedef struct {
//   uint8_t voice;
//   uint8_t chan;
//   uint8_t bar;
//   uint8_t note;
//   uint8_t mute;
//   steps_t steps[64];
// } device_t;

// extern device_t devices[8];

int clientIPCheck ( char myArray[] );

extern "C"{
    void initUdpServerTask();
    void udp_server_task(void *pvParameters);
    }

#endif
