#ifndef WIFI_MDNS_H
#define WIFI_MDNS_H

void initialise_mdns(void);
extern "C" {
    void wifi_init_sta(void);
    void smartconfig_example_task(void * parm);
}
#endif