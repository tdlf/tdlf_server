# technologies-de-la-fete Server
ESP32 Ableton Link Midi clock, sensor interface and modular sequencer.

Wiki : https://github.com/alx-1/technologies-de-la-fete/wiki

In 'main/CMakeLists.txt', edit the path to your link directory for example : include(/../../link/AbletonLinkConfig.cmake)

The link to esp32 adaptation is by Mathias Bredholt (https://github.com/mathiasbredholt/link/tree/master/include/ableton) Thank you!  The following is from his README

# *E X P E R I M E N T A L* 

*Tested with esp-idf [v4.0-1](https://github.com/espressif/esp-idf/releases/tag/v4.0-rc)*

## ABLETON LINK -> MIDI

Sending Midi Clock and Start / Stop (uart - midi connector) based on Ableton Link session.
WIP

## Building and Running the Example

* Setup esp-idf as described in [the documentation](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/index.html)
* Run `idf.py menuconfig` and setup WiFi credentials under 
`Example Connection Configuration` + Serial flasher config -> Flash size (4 MB) + Partition Table == Custom partition table CSV
```
idf.py build
idf.py -p ${ESP32_SERIAL_PORT} flash
```

## The representation of notes, steps, midi channel, etc.

```
device n  |-- author
          |-- date
          |-- train
          |-- voice (0-7)
          |-- midi channel (1-16)
          |-- bar (0-3)
          |-- midi note number (ex. bass drum == 36)
          |-- mute state (on/off)
          |-- step[64]|
                         |-- on/off
                         |-- note length
                         |-- note velocity
                         |-- ratch
```
The sequence information is kept in a 8 value array of type 'device'. A device is a struct containing the voice number, midi channel, bar, midi note number, mute step information as well as an array of 64 steps, themselves a 'step' struct containing note on or off value, note length, note velocity and note ratch information.
```
///// step /////
typedef struct {
    bool on;          
    uint8_t length;
    uint8_t vel;
    uint8_t ratch;
} steps_t;

typedef struct {
    uint16_t year;  // e.g., 2025
    uint8_t month;  // 1-12
    uint8_t day;    // 1-31
} date_t;

///// device /////
typedef struct {
    char author[20]; 
    date_t date;
    bool train;
    uint8_t voice;
    uint8_t chan;
    uint8_t bar;
    uint8_t note;
    bool mute;
    steps_t steps[64];
} device_t;

device_t devices[8];
```

